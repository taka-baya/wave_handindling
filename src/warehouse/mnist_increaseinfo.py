import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.datasets import tuple_dataset

import sys

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

N_EPOCH = 100

class Autoencoder(chainer.Chain):
    def __init__(self):
        super(Autoencoder, self).__init__(
                encoder = L.Linear(784, 64),
                decoder = L.Linear(64, 784))

    def __call__(self, x):
        h = F.relu(self.encoder(x))
        return F.relu(self.decoder(h))

class Makde_dataset(module):
    def __init__(self)

def plot_mnist_data(samples):
    for index, (data, label) in enumerate(samples):
        plt.subplot(4, 4, index + 1)
        plt.axis('off')
        plt.imshow(data.reshape(28, 28), cmap=cm.gray_r, interpolation='nearest')
        n = int(label)
        plt.title(n, color='red')
    plt.savefig("./pict/epoch_"+str(N_EPOCH)+'.png')
    plt.show()

def main():
    train, test = chainer.datasets.get_mnist()
    test = test[47:63]

    print("a1")
    model = L.Classifier(Autoencoder(), lossfun=F.mean_squared_error)
    print("a2")
    chainer.serializers.load_npz("./test_result/model_final.npz", model)
    print("a1")

    pred_list = []
    for (data, label) in test:
        pred_data = model.predictor(np.array([data]).astype(np.float32)).data
        print(pred_data)
        pred_list.append((pred_data, label))
    plot_mnist_data(pred_list)

if __name__ == "__main__":
    main()
