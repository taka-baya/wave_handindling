import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.datasets import tuple_dataset

import sys

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import os

N_EPOCH = 10

class Autoencoder(chainer.Chain):
    def __init__(self):
        super(Autoencoder, self).__init__(
                encoder = L.Linear(784, 64),
                decoder = L.Linear(64, 784))

    def __call__(self, x, hidden=False):
        h = F.relu(self.encoder(x))
        if hidden:
            return h
        else:
             return F.relu(self.decoder(h))

class Makedataset(chainer.dataset.DatasetMixin):
    def __init__(self, inputdata, outputdata):
        self._input = inputdata
        self._output = outputdata

    def __len__(self):
        return len(self._input)

    def get_example(self, i):
        return (self._input[i], self._output[i])

train, test = chainer.datasets.get_mnist()
train = train[0:10000]
#print(train[1])
test = test[47:63]
#plot_mnist_data(test)

train = [i[0] for i in train]
test = [i[0] for i in test]

train_dataset = Makedataset(train,train)
test_dataset = Makedataset(test,test)
# train = tuple_dataset.TupleDataset(train, train)
train_iter = chainer.iterators.SerialIterator(train_dataset, 100 ,True ,True)
itr_test = chainer.iterators.SerialIterator(test_dataset, len(test_dataset), False, False)

model = L.Classifier(Autoencoder(), lossfun=F.mean_squared_error)
model.compute_accuracy = False
optimizer = chainer.optimizers.Adam(0.01)
optimizer.setup(model)

updater = training.StandardUpdater(train_iter, optimizer, device=-1)
trainer = training.Trainer(updater, (N_EPOCH, 'epoch'), out="result")

trainer.extend(extensions.Evaluator(itr_test, model))

trainer.extend(extensions.LogReport())
trainer.extend(extensions.PrintReport( ['epoch', 'main/loss']))
trainer.extend(extensions.ProgressBar())

trainer.run()

result_folder = "test_result"
dir_path ="./"+result_folder
if not os.path.isdir(dir_path):
    os.makedirs(dir_path)

chainer.serializers.save_npz("./{0}/model_final.npz".format(result_folder), model)

#pred_list = []
#for (data, label) in test:
#    pred_data = model.predictor(np.array([data]).astype(np.float32)).data
#    pred_list.append((pred_data, label))
#plot_mnist_data(pred_list)