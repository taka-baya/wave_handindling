import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.datasets import tuple_dataset
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import wave
import os
from scipy import fromstring, int16 ,int32
import scipy.signal as sps
import pandas as pd
import argparse
from chainer import Variable

## chainerの学習ネットワークの定義層
class Autoencoder(chainer.Chain):
	def __init__(self):
		super(Autoencoder, self).__init__(
			l1 = L.Linear(17640, 32000),
			l2 = L.Linear(32000, 44100),
			l3 = L.Linear(44100, 88200))

	def __call__(self, x, hidden=False):
		h1 = F.dropout(F.leaky_relu(self.l1(x)),ratio=0.2)
		h2 = F.dropout(F.leaky_relu(self.l2(h1)),ratio=0.2)
		return F.relu(self.l3(h2))

class Autoencoder3(chainer.Chain):
    def __init__(self):
        super(Autoencoder3, self).__init__(
                encoder = L.Linear(17640, 6400),
                decoder = L.Linear(6400, 88200))

    def __call__(self, x, hidden=False):
        h = F.relu(self.encoder(x))
        if hidden:
            return h
        else:
             return F.relu(self.decoder(h))

## csvファイルの読み込み、書き込み
class file_treatment2(object):
	"""docstring for file_treatment"""
	def __init__(self, folder_path):
		self.folder_path = folder_path

	def __call__(self):
		if not os.path.isdir(self.folder_path):
			os.makedirs(self.folder_path)

	## csvファイル書き込み処理関数
	def write_to_csv(self, file_name, write_data):
		print("write csv file :{}".format(self.folder_path + file_name))
		write_data.to_csv(self.folder_path + file_name)
	
	## csvファイル読み込み処理
	def read_csv_file(self, file_name):
		print("read csv file :{}".format(self.folder_path + file_name))
		read_df = pd.read_csv(self.folder_path + file_name)

		return read_df

## csvファイルの読み込み、書き込み
class check_result(object):
    """docstring for file_treatment"""
    def __init__(self, train_csv_path, ans_csv_path, out_wave_file_path, model_file_path="./results", model_fn = "model_final.npz"):
        self.train_csv_path = train_csv_path
        self.ans_csv_path = ans_csv_path
        self.out_wave_file_path = out_wave_file_path
        self.model_file_path = model_file_path
        self.model_fn = model_fn
    
    def __call__(self):
        if not os.path.isdir(self.out_wave_file_path):
            os.makedirs(self.out_wave_file_path)
    
    def show_model(self, num, train_file_name,ans_file_name):
        model = L.Classifier(Autoencoder(), lossfun=F.mean_squared_error)
        chainer.serializers.load_npz("{0}/{1}".format(self.model_file_path,self.model_fn), model)
        print(model)

        train_data = []
        with chainer.using_config('train', False):
            for i in range(num , num + 2):
                train_file = str(i) + train_file_name
                ftt = file_treatment2(self.train_csv_path)
                train_data_df = ftt.read_csv_file(train_file)
                train_data_np = np.array(train_data_df['0']*10000)
                train_data.append(train_data_np)
        
        print(train_data)
        train_data_v = np.array(train_data)
        #train_data_v = Variable(train_data)
        #print(train_data.shape)
        #with chainer.using_config('train',False):
        #    out_data = model.predictor(train_data.astype(np.float32)).train_data
        
        #y1 = model(train_data_v)
        #g = c.build_computational_graph([y1])
        #with open('graph.dot', 'w') as o:
        #    o.write(g.dump())

        out_data = []
        for data in train_data_v:
            pred_data = model.predictor(np.array([data]).astype(np.float32)).data
            out_data.append(pred_data)

        return out_data
    
    def show_log(self):
        pass


def main():
    train_csv_path = "../data/train_data_csv/"
    ans_csv_path = "../data/ans_data_csv/"
    out_wave_file_path = "../outwave_file/"
    train_extension = "_train_csv_data.csv"
    answer_extension = "_ans_csv_data.csv"
    wav_number = 1001
    print_info = False

    cam = check_result(train_csv_path, ans_csv_path, out_wave_file_path)
    data = cam.show_model(wav_number, train_extension, answer_extension)

    print(data)

    text_name = 'test.txt'
    with open(text_name, mode='w') as f:
        f.write(str(data))

if __name__ == "__main__":
    main()