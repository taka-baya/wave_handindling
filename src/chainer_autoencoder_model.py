import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.datasets import tuple_dataset
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import wave
import os
from scipy import fromstring, int16 ,int32
import scipy.signal as sps
import pandas as pd
import argparse

## csvファイルの読み込み、書き込み
class file_treatment(object):
	"""docstring for file_treatment"""
	def __init__(self, folder_path):
		self.folder_path = folder_path

	def __call__(self):
		if not os.path.isdir(self.folder_path):
			os.makedirs(self.folder_path)

	## csvファイル書き込み処理関数
	def write_to_csv(self, file_name, write_data):
		print("write csv file :{}".format(self.folder_path + file_name))
		write_data.to_csv(self.folder_path + file_name)
	
	## csvファイル読み込み処理
	def read_csv_file(self, file_name):
		print("read csv file :{}".format(self.folder_path + file_name))
		read_df = pd.read_csv(self.folder_path + file_name)

		return read_df
		

## wavファイルの読み書き・正規化処理
class read_wavefile(object):
	"""docstring for read_wavefile"""
	def __init__(self, folder_path, extension, wav_number):
		self.extension = extension
		self.folder_path = folder_path
		self.wav_number = wav_number

	def __call__(self, print_info):
		read_wave_data = []
		for i in range(0,self.wav_number):
			file_name = self.folder_path + str(i) + self.extension

			read_wave_data.append(self.read_wave_file(file_name,print_info))
			print("read wave file : {0}".format(str(file_name)))

		return np.array(read_wave_data)

	## .wavファイル読み込み処理
	def read_wave_file(self, wave_filename, print_info):
		wavefile = wave.open(wave_filename, 'rb')

		if print_info:
			print("オーディオチャンネル数（モノラルなら 1 、ステレオなら 2 ） : {0}".format(wavefile.getnchannels()))
			print("サンプルサイズをバイト数で返します。　　　　　　　　　　　  : {0}".format(wavefile.getsampwidth()))
			print("サンプリングレートを返します。　　　　　　     　　　　　  : {0}".format(wavefile.getframerate()))
			print("オーディオフレーム数を返します。　　　　　     　　　　　  : {0}".format(wavefile.getnframes()))
			print("圧縮形式を返します（ 'NONE' だけがサポートされている形式です）。: {0}".format(wavefile.getcomptype()))
			print("get*() メソッドが返すのと同じ 　　　　　     　　　　　  : {0}".format(wavefile.getparams()))
			print("録音時間                                            : {0}".format(wavefile.getnframes()/wavefile.getframerate()))

		if wavefile.getsampwidth() == 2:
			r_data = fromstring(wavefile.readframes(wavefile.getnframes()), dtype = int16)
		if wavefile.getsampwidth() == 4:
			r_data = fromstring(wavefile.readframes(wavefile.getnframes()), dtype = int32)

		return r_data

	## データ正規化処理その1
	def nomalize_data(self, wave_data, number):
		normaraize_train_data = []
		ave_memorize = []
		var_memorize = []

		for i in range(0,number):
			ave = np.average(wave_data[i])
			var = np.var(wave_data[i])
			ave_memorize.append(ave)
			var_memorize.append(var)
			normaraize_train_data.append((wave_data[i] - ave)/var)

		return normaraize_train_data, ave_memorize ,var_memorize

	## データ正規化処理その2
	def nomalize_data2(self, wave_data, number):
		normaraize_train_data = []

		ave = np.average(wave_data)
		var = np.var(wave_data)

		print("ave , var : {0},{1}".format(ave,var))

		for i in range(0,number):
			normaraize_train_data.append((wave_data[i] - ave)/var)

		return normaraize_train_data, ave ,var

	## 正規化処理したデータを元の音声データに復号化
	def decode_nomalized_data(self, normaraize_train_data, ave_memorize, var_memorize ,number):
		decode_wave_file = []

		for i in range(0,number):
			decode_wave_file.append((normaraize_train_data[i] * var_memorize[i]) +ave_memorize[i])

		return decode_wave_file

## chainerの学習ネットワークの定義層
class Autoencoder(chainer.Chain):
	def __init__(self):
		super(Autoencoder, self).__init__(
			l1 = L.Linear(17640, 32000),
			l2 = L.Linear(32000, 44100),
			l3 = L.Linear(44100, 88200))

	def __call__(self, x, hidden=False):
		h1 = F.dropout(F.leaky_relu(self.l1(x)),ratio=0.2)
		h2 = F.dropout(F.leaky_relu(self.l2(h1)),ratio=0.2)
		return F.relu(self.l3(h2))

## chainerの学習ネットワークの定義層
class Autoencoder2(chainer.Chain):
	def __init__(self):
		super(Autoencoder2, self).__init__(
			l1 = L.Linear(17640, 6400),
			l2 = L.Linear(6400, 88200))

	def __call__(self, x, hidden=False):
		h1 = F.dropout(F.leaky_relu(self.l1(x)),ratio=0.2)
		return F.relu(self.l2(h1))

class Autoencoder3(chainer.Chain):
    def __init__(self):
        super(Autoencoder3, self).__init__(
                encoder = L.Linear(17640, 6400),
                decoder = L.Linear(6400, 88200))

    def __call__(self, x, hidden=False):
        h = F.relu(self.encoder(x))
        if hidden:
            return h
        else:
             return F.relu(self.decoder(h))

## 学習用データセットの作成
class CreateLearningDataset(chainer.dataset.DatasetMixin):
	def __init__(self, train_data, ans_data):
		self._inputdata = train_data
		self._outputdata = ans_data
	
	def __len__(self):
		return len(self._inputdata)
	
	def get_example(self, i):
		return (self._inputdata[i], self._outputdata[i])

## chainerの学習実行処理部分
def execute_training(args,train_data,answer_data):

	if not os.path.exists(args.out):
		os.makedirs(args.out)

	# 学習用データの作成
	dataset = CreateLearningDataset(train_data,answer_data)
	train,test = chainer.datasets.split_dataset(dataset, (int)(len(dataset)*.9))

	itr_train = chainer.iterators.SerialIterator(train, args.batchsize, True, True)
	itr_test = chainer.iterators.SerialIterator(test, len(test), False, False)

	model = L.Classifier(Autoencoder(), lossfun=F.mean_squared_error)
	model.compute_accuracy = False
	optimizer = chainer.optimizers.Adam(args.lr)
	optimizer.setup(model)

	#updater = training.updaters.StandardUpdater(itr_train, opt, device=args.gpu)
    #trainer = training.Trainer(updater, (args.epoch, "epoch"), out=args.out)
	#updater = training.StandardUpdater(train_iter, optimizer, device=-1)
	#trainer = training.Trainer(updater, (N_EPOCH, 'epoch'), out="result")

	updater = training.updaters.StandardUpdater(itr_train, optimizer,device=-1)
	trainer = training.Trainer(updater, (args.epoch, "epoch"), out=args.out)

	trainer.extend(extensions.Evaluator(itr_test, model))

	trainer.extend(extensions.LogReport())
	trainer.extend(extensions.PlotReport(['main/loss', 'validation/main/loss'], x_key='epoch', file_name='loss.png'))
	trainer.extend(extensions.ProgressBar())

	trainer.run()
	model.to_cpu()

	chainer.serializers.save_npz("./{0}/model_final.npz".format(args.out), model)

## csvファイルから学習データの読み込み
def get_data(file_path,extension,wav_number):
	ft_class = file_treatment(file_path)
	ft_class()
	data = []

	for i in range(0,wav_number):
		data_df = ft_class.read_csv_file(str(i) + extension)
		data.append(np.array(data_df['0']*10000))
	
	return data

## main処理
def main():
	train_file_path = "../data/train_data_csv/"
	train_extension = "_train_csv_data.csv"
	answer_file_path = "../data/ans_data_csv/"
	answer_extension = "_ans_csv_data.csv"
	wav_number = 1000
	epoch = 100
	batch_size = 100
	#print_info = False

	ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	ap.add_argument("--dataset_ipath", "-ipath", type=str, default=train_file_path, help="Dataset input node file path.")
	ap.add_argument("--dataset_iname", "-iname", type=str, default=train_extension, help="Dataset input node file name.")
	ap.add_argument("--dataset_opath", "-opath", type=str, default=answer_file_path, help="Dataset out node file path.")
	ap.add_argument("--dataset_oname", "-oname", type=str, default=answer_extension, help="Dataset out node file name.")
	ap.add_argument("--datasize", "-n", type=int, default=wav_number, help="wave number")
	ap.add_argument("--batchsize", "-b", type=int, default=batch_size, help="Batchsize")
	ap.add_argument("--epoch", "-e", type=int, default=epoch, help="Number of epochs")
	ap.add_argument("--out", "-o", type=str, default="results", help="Output directory name")
	ap.add_argument("--gpu", "-g", type=int, default=-1, help="GPU ID")
	ap.add_argument("--lr", "-l", type=float, default=0.01, help="Learning rate of SGD")

	args = ap.parse_args()

	# 学習データの読み込み
	train_data = np.array(get_data(args.dataset_ipath,args.dataset_iname,args.datasize))
	ans_data = np.array(get_data(args.dataset_opath,args.dataset_oname,args.datasize))

	print("train data shape :{0}".format(train_data.shape))
	print("train data shape :{0}".format(ans_data.shape))

	execute_training(args, train_data.astype(np.float32) ,ans_data.astype(np.float32))


if __name__ == '__main__':
	main()
