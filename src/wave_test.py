import os
import wave

wave_filename = "1.wav"
wave_filepath = "../sample_data/"

writewavefile = "1_write.wav"

wavefile = wave.open(wave_filepath + wave_filename, 'rb')
print("オーディオチャンネル数（モノラルなら 1 、ステレオなら 2 ） : {0}".format(wavefile.getnchannels()))
print("サンプルサイズをバイト数で返します。　　　　　　　　　　　  : {0}".format(wavefile.getsampwidth()))
print("サンプリングレートを返します。　　　　　　     　　　　　  : {0}".format(wavefile.getframerate()))
print("オーディオフレーム数を返します。　　　　　     　　　　　  : {0}".format(wavefile.getnframes()))
print("圧縮形式を返します（ 'NONE' だけがサポートされている形式です）。: {0}".format(wavefile.getcomptype()))
print("get*() メソッドが返すのと同じ 　　　　　     　　　　　  : {0}".format(wavefile.getparams()))


#wavefile = wave.open(wave_filepath + writewavefile, 'wb')
#writewave = wavefile
#writewave.setframerate(11025)