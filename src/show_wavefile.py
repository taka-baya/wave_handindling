import numpy as np
import wave
import matplotlib.pyplot as plt
from scipy import fromstring, int16 ,int32

class show_wavefile(object):
	"""docstring for show_wavefile"""
	def __init__(self, file_name):
		self.file_name = file_name

	def plot_data(self,data):
		plt.plot(data)
		plt.show()

	def read_wave_file(self,print_info):
		wavefile = wave.open(self.file_name, 'rb')

		if print_info:
			print("オーディオチャンネル数（モノラルなら 1 、ステレオなら 2 ） : {0}".format(wavefile.getnchannels()))
			print("サンプルサイズをバイト数で返します。　　　　　　　　　　　  : {0}".format(wavefile.getsampwidth()))
			print("サンプリングレートを返します。　　　　　　     　　　　　  : {0}".format(wavefile.getframerate()))
			print("オーディオフレーム数を返します。　　　　　     　　　　　  : {0}".format(wavefile.getnframes()))
			print("圧縮形式を返します（ 'NONE' だけがサポートされている形式です）。: {0}".format(wavefile.getcomptype()))
			print("get*() メソッドが返すのと同じ 　　　　　     　　　　　  : {0}".format(wavefile.getparams()))
			print("録音時間                                            : {0}".format(wavefile.getnframes()/wavefile.getframerate()))

		buf_data = wavefile.readframes(wavefile.getnframes())
		s_width = wavefile.getsampwidth()
		ch = wavefile.getnchannels()

		if s_width == 2:
			r_data = fromstring(buf_data, dtype = int16)
		if s_width == 4:
			r_data = fromstring(buf_data, dtype = int32)

		if (ch == 2):
			# 左チャンネル
			left_data = r_data[::2]
			# 右チャンネル
			right_data = r_data[1::2]

		return left_data,right_data


file_name = "../train_data/1_t.wav"
print_info = True
sw = show_wavefile(file_name)
l,r = sw.read_wave_file(print_info)
sw.plot_data(l)

		