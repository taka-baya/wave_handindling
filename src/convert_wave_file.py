import os
import wave
import numpy as np
from scipy import fromstring, int16 ,int32
import matplotlib.pyplot as plt
import scipy.signal as sps

a_data_number = 0 # グローバル変数
t_data_number = 0 # グローバル変数

def read_wave_file(wave_filepath,wave_filename):
	wavefile = wave.open(wave_filepath + wave_filename, 'rb')

	print_j = False
	if print_j:
		print("オーディオチャンネル数（モノラルなら 1 、ステレオなら 2 ） : {0}".format(wavefile.getnchannels()))
		print("サンプルサイズをバイト数で返します。　　　　　　　　　　　  : {0}".format(wavefile.getsampwidth()))
		print("サンプリングレートを返します。　　　　　　     　　　　　  : {0}".format(wavefile.getframerate()))
		print("オーディオフレーム数を返します。　　　　　     　　　　　  : {0}".format(wavefile.getnframes()))
		print("圧縮形式を返します（ 'NONE' だけがサポートされている形式です）。: {0}".format(wavefile.getcomptype()))
		print("get*() メソッドが返すのと同じ 　　　　　     　　　　　  : {0}".format(wavefile.getparams()))
		print("録音時間                                            : {0}".format(wavefile.getnframes()/wavefile.getframerate()))

	return wavefile.getnchannels(),wavefile.getsampwidth(),wavefile.getframerate(),wavefile.getnframes(),wavefile.readframes(wavefile.getnframes())

def plot_left_right(left,right):
	plt.plot(left)
	plt.plot(right)
	plt.show()


def write_wave_file(wave_filepath,writewavefile,ch,width,fr,outd):
	ww = wave.open(wave_filepath + writewavefile, 'w')

	ww.setnchannels(ch)
	ww.setsampwidth(width)
	ww.setframerate(fr)
	ww.writeframes(outd)
	ww.close()

def my_mkdir(file_path):
	if not os.path.exists(file_path):
		os.makedirs(file_path)

def handling_wave_file2(read_wave_filepath,extension,ansewer_out_filepath,training_out_filepath,down_sample_rate):
	global a_data_number

	for i in range(0,7866):
		ch ,s_width ,f_rate ,f_num ,buf_data = read_wave_file(read_wave_filepath,str(i) + "_t" + extension)

		if s_width == 2:
			r_data = fromstring(buf_data, dtype = int16)
		if s_width == 4:
			r_data = fromstring(buf_data, dtype = int32)
		
		training_data = np.array(r_data[::5])

		training_data_filename = str(a_data_number) + "_t" + extension
		write_wave_file(training_out_filepath,training_data_filename,ch,s_width,down_sample_rate,training_data)

		a_data_number += 1

def handling_wave_file(read_wave_filepath,extension,ansewer_out_filepath,training_out_filepath,down_sample_rate):
	global a_data_number
	time = 10 # 10秒
	frames_number = 44100 * time * 2

	for i in range(1,204):
		ch ,s_width ,f_rate ,f_num ,buf_data = read_wave_file(read_wave_filepath,str(i) + extension)

		if s_width == 2:
			r_data = fromstring(buf_data, dtype = int16)
		if s_width == 4:
			r_data = fromstring(buf_data, dtype = int32)

		cut_num = int(f_num/(f_rate * 10))

		print("data number {0} ,cut number {1}".format(i,cut_num))

		#if (ch == 2):
			## 左チャンネル
			#left_data = r_data[::2]
			## 右チャンネル
			#right_data = r_data[1::2]

		for j in range(0,cut_num):
			write_data = np.array(r_data[:frames_number])
			r_data = np.delete(r_data, range(frames_number))

			# 正解データの書き込み
			answer_data_filename = str(a_data_number) + "_a" + extension
			write_wave_file(ansewer_out_filepath,answer_data_filename,ch,s_width,f_rate,write_data)

			# 学習用データの作成と書き込み
			training_data = np.array(write_data[::10])
			training_data_filename = str(a_data_number) + "_t" + extension
			write_wave_file(training_out_filepath,training_data_filename,ch,s_width,down_sample_rate,training_data)

			a_data_number += 1

def main():
	extension = ".wav" # 拡張子
	read_wave_filepath = "../train_data1/" #読み込む音声データのファイルパス
	ansewer_out_filepath = "../ans_data/"
	training_out_filepath = "../train_data/"

	my_mkdir(ansewer_out_filepath)
	my_mkdir(training_out_filepath)

	#down_sample_rate = 4410 #落とすサンプリングレート
	down_sample_rate = 882 #落とすサンプリングレート

	handling_wave_file2(read_wave_filepath,extension,ansewer_out_filepath,training_out_filepath,down_sample_rate)

	print("finish !!!")

if __name__ == '__main__':
	main()
