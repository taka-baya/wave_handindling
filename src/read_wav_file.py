import numpy as np
from scipy.io.wavfile import read
from matplotlib import pyplot as plt
from scipy.io.wavfile import write

class file_read:
	"""docstring for ClassName"""
	def __init__(self, file_path):
		self.file_path = file_path
		print("read {0} data".format(self.file_path))

	def __call__(self, file_path):
		rate, data = read(file_path)
		return rate, data

def create_noise(noise_rate,num):
	y1 = np.random.normal(0, 1, size=num)
	y2 = create_sin(num)
	noise = np.array((y1 + y2)*noise_rate)
	return noise

def create_sin(num):
	fs = 8000 #サンプリング周波数
	f0 = 35 #周波数
	a = 3 #振幅

	swav = []

	for n in np.arange(num):
    	#サイン波を生成
		s = a * np.sin(2.0 * np.pi * f0 * n / fs)
		swav.append(s)
	return swav
		
def main():
	read_path = "./piano_data/"
	#read_path = "./sample/20khz/"
	read_path1 = "./sample/20khz/"
	extension = ".wav"
	filename = read_path + "1" + extension

	filename1 = read_path1 + "1" + extension

	noise_rate=2000

	r = file_read(filename)
	rate ,data =r(filename)
	print(rate)
	print(len(data))


	r1 = file_read(filename1)
	rate1 ,data1 =r(filename1)
	print(rate1)
	print(len(data))

	# noise_x = create_noise(noise_rate,len(data[:,0]))


	#noise_left = data[:,0]+noise_x
	#noise_right = data[:,1]+noise_x

	#noise_data = np.c_[noise_left,noise_right]
	
	#noise_data = noise_data/50000

	#print(noise_data[3200000])

	#write('out.wav', rate, noise_data)
	plt.xlim(4000000, 6000000)
	plt.plot(data1[:,0],label = '22khz')
	plt.plot(data[:,0],label = '44khz')

	plt.tick_params(labelsize=14)
	plt.grid(True)
	plt.legend(loc = 'upper right')
	plt.show()

if __name__ == '__main__':
	main()