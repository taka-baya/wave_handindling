FROM python:3.6

RUN DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata

ENV TZ=Asia/Tokyo

RUN apt-get install -y vim
RUN pip3 install numpy scipy pandas matplotlib chainer